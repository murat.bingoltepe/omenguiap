import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './layouts/main/main.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { ProductsComponent } from './modules/product/products/products.component';
import { AppSettingsComponent } from './modules/settings/app-settings/app-settings.component';
import { UserSettingsComponent } from './modules/settings/user-settings/user-settings.component';
import { LoginComponent } from './modules/user/login/login.component';
import { LogoutComponent } from './modules/user/logout/logout.component';
import { UserRegisterComponent } from './modules/user/userregister/userregister.component';
import { CustomersComponent } from './modules/customer/customers/customers.component';
import { UsersComponent } from './modules/user/users/users.component';

const routes: Routes = [{
  path: '',
  component: MainComponent,
  children: [{
    path: '',
    component: DashboardComponent
  }, {
    path: 'products',
    component: ProductsComponent
  }, {
    path: 'app-settings',
    component: AppSettingsComponent
  }, {
    path: 'user-settings',
    component: UserSettingsComponent
  }, {
    path: 'user-register',
    component: UserRegisterComponent
  }, {
    path: 'user-login',
    component: LoginComponent
  }, {
    path: 'user-logout',
    component: LogoutComponent
  }, {
    path: 'customers',
    component: CustomersComponent
  }, {
    path: 'users',
    component: UsersComponent
  }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
