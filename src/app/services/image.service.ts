import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor() { }

  imageExists(urlToFile: string) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', urlToFile, false);
    xhr.send();

    if (xhr.status == 200) {
      return true;
    } else {
      return false;
    }
  }

  getProductImageSrc(productId: number): string {
    let imagePath: string = "./assets/img/products/thumbs/" + productId + ".png";
    if (this.imageExists(imagePath)) {
      return imagePath;
    }
    else {
      return "./assets/img/products/thumbs/0.png";
    }
  };

}
