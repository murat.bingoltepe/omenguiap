import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { LoginUser } from 'src/app/models/loginUser';
import { RegisterUser } from 'src/app/models/registerUser';
import { Router } from '@angular/router';
import { BackendBaseService } from './backend-base.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private backendService: BackendBaseService
  ) { }

  path = this.backendService.getAppSettings().AppSettings.BaseUrl + '/api/employeeauth/login';

  userToken: any;
  decodedToken: any;
  jwtHelper: JwtHelperService = new JwtHelperService();
  TOKEN_KEY = 'token';

  login(loginUser: LoginUser) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    this.httpClient
      .post(this.path, loginUser, { headers: headers })
      .subscribe(data => {
        this.saveToken(data);
        this.userToken = data;
        this.decodedToken = this.jwtHelper.decodeToken(data.toString());
        console.log('Login to the system successfully');
        this.router.navigate(['pages/login']);
      });
  }

  register(registerUser: RegisterUser) {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');

    this.httpClient
      .post(this.path + 'register', registerUser, { headers: headers })
      .subscribe(data => { },
        (error: HttpErrorResponse) => {
        }
      );
  }

  saveToken(token: any) {
    localStorage.setItem(this.TOKEN_KEY, token);
  }

  logOut() {
    localStorage.removeItem(this.TOKEN_KEY);
    console.log('Logout from the system successfully');
    this.router.navigate(['pages/login']);
  }

  loggedIn() {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  get token() {
    return localStorage.getItem(this.TOKEN_KEY);
  }
  /* getCurrentUserId() {
    return this.jwtHelper.decodeToken(this.token).nameid;
  }*/

  /* getUser() {
    return this.jwtHelper.decodeToken(this.token);
  } */

}
