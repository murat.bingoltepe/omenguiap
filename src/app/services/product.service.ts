import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendBaseService } from './backend-base.service';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})

export class ProductService {

  constructor(
    private httpClient: HttpClient
  ) { }

  path = 'https://localhost:44356/api/';

  getProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.path + 'products');
  }

}
