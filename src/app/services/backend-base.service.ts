import { Injectable } from '@angular/core';
import * as AppSettings from 'src/assets/omeng/orderSettings.json'

@Injectable({
  providedIn: 'root'
})

export class BackendBaseService {

  constructor() { }

  appSettting: any = AppSettings;

  getAppSettings() {
    return this.appSettting;
  }

}
