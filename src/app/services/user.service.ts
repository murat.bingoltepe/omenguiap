import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendBaseService } from './backend-base.service';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee';
import { Customer } from '../models/customer';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
    private backendService: BackendBaseService
  ) { }
  pathEmployee = this.backendService.getAppSettings().AppSettings.BaseUrl + '/api/employees/';
  pathCustomer = this.backendService.getAppSettings().AppSettings.BaseUrl + '/api/customers/';

  // Employee
  getEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.pathEmployee);
  }
  getEmployeeByRoleId(employeeId: number): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.pathEmployee + 'detail/ByRId/?=' + employeeId);
  }
  getEmployeeById(employeeId: number): Observable<Employee> {
    return this.httpClient.get<Employee>(this.pathEmployee + 'detail/ByEId/?=' + employeeId);
  }
  saveEmployee(employee: Employee) {
    this.httpClient.post(this.pathEmployee + 'update', employee).subscribe();
  }
  deleteEmployee(employee: Employee) {
    this.httpClient.delete(this.pathEmployee + 'delete/byeid/?id=' + employee.id).subscribe(data => {
    });
  }
  addEmployee(user: User) {
    this.httpClient.post(this.pathEmployee + 'add', user).subscribe();
  }

  // Customer
  getCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.pathCustomer);
  }
  getCustomerByRoleId(customerId: number): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(this.pathCustomer + 'detail/ByRId/?=' + customerId);
  }
  getCustomerById(customerId: number): Observable<Customer> {
    return this.httpClient.get<Customer>(this.pathCustomer + 'detail/ByCId/?=' + customerId);
  }
  saveCustomer(customer: Customer) {
    this.httpClient.post(this.pathCustomer + 'update', customer).subscribe();
  }
  deleteCustomer(customer: Customer) {
    this.httpClient.delete(this.pathCustomer + 'delete/ByCId/?id=' + customer.id).subscribe();
  }
  addCustomer(user: User) {
    this.httpClient.post(this.pathCustomer + 'add', user).subscribe();
  };


}
