import { Component } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})

export class ProductsComponent {
  displayedColumns: string[] = ['id', 'name'];
  myDataArray = this.productService.getProducts();

  constructor(
    private productService: ProductService
  ) { }

  /* ngOnInit(): void {
    this.productService.getProducts().subscribe(data => {
      data.forEach((product): void => {
        this.myDataArray.push(product);
      });
    });
  } */

}
