import { Address } from './address';
import { Account } from './account';
import { UserRole } from './userRole';
import { Person } from './person';

export interface User extends Person {
    address: Address;
    account: Account;
    userRole: UserRole;
}