import { User } from './user';

export interface Customer extends User {
    socialSecurityNumber: string;
    taxId: string;
    companyName: string;
    positionByCompany: string;
}