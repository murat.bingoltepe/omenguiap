export class Category {
    constructor (
    public id: number,
    public parentId: number,
    public name: string,
    public description: string,
    public isParent: boolean
    ) {}
}