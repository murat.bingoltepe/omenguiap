import { DecimalPipe } from '@angular/common';
import { Photo } from './photo';
import { Employee } from './employee';

export interface Product {
    id: number,
    name: string,
    description: string,
    quantityPerUnit: string,
    unitPrice: DecimalPipe,
    employee: Employee,
    categoryId: number,
    photos: Photo[],
    addedTimeStamp: string,
    updatedTimeStamp: string
}