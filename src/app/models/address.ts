export interface Address {
    id: number;
    street: string;
    number: number;
    postalCode: string;
    city: string;
    country: string;
}