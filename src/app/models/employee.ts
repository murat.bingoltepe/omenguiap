import { User } from './user';

export interface Employee extends User {
    socialSecurityNumber: string;
}
