export interface Account {
    id: number;
    iban: string;
    swift: string;
    bankName: string;
    bankCode: string;
    accountNumber: string;

}