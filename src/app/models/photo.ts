import { Product } from './product';
import { Employee } from './employee';

export interface Photo {
    id: number;
    description: string;
    addedTimeStamp: Date;
    isMain: boolean;
    path: string;
    productId: number;
    product: Product;
    employeeId: number;
    employee: Employee;
}
