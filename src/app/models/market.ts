import { Address } from './address';

export interface Market {
    id: number;
    name: string;
    contactPerson: string;
    status: string;
    address: Address;
}