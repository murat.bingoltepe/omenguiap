import { OrderState } from 'src/app/enums/order';

export interface Order {
    id: number;
    userId: number;
    dateTime: Date;
    state: OrderState;
    desc: string;
}