export interface Person {
    id: number;
    userName: string;

    tempPassword: boolean;
    firstName: string;
    lastName: string;
    title: string;
    gender: string;
    birthDate: Date;
    tel: string;
    emailConfirmed: boolean;
    addedTimeStamp: Date;
    updatedTimeStamp: Date;
    discriminator: string;
}