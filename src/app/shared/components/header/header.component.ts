import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ThemeService } from 'src/app/services/theme.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();

  protected isDarkMode;
  lightModeIcon = 'light_mode';
  darkModeIcon = 'brightness_3';
  iconSymbol;

  constructor(private themeService: ThemeService) { 
    this.themeService.initTheme();
    this.isDarkMode = this.themeService.isDarkMode();
    this.iconSymbol = this.isDarkMode ? this.lightModeIcon : this.darkModeIcon;
  }

  ngOnInit(): void {
  }

  toggleSideBar() {
    this.toggleSideBarForMe.emit();

    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }

  changeTheme() {
    this.isDarkMode = this.themeService.isDarkMode();
    if (this.isDarkMode) {
      this.themeService.update('lightMode');
      this.iconSymbol = this.darkModeIcon;
    } else {
      this.themeService.update('darkMode');
      this.iconSymbol = this.lightModeIcon;
    }
    
    //this.iconSymbol = this.isDarkMode ? this.lightModeIcon : this.darkModeIcon;
  }

}
