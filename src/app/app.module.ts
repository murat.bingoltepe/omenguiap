import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainModule } from './layouts/main/main.module';
import { LoginComponent } from './modules/user/login/login.component';
import { LogoutComponent } from './modules/user/logout/logout.component';
import { ProductsComponent } from './modules/product/products/products.component';
import { ProductUpdateComponent } from './modules/product/product-update/product-update.component';
import { MatTableModule } from '@angular/material/table';
import { CustomersComponent } from './modules/customer/customers/customers.component';
import { UsersComponent } from './modules/user/users/users.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    ProductsComponent,
    ProductUpdateComponent,
    CustomersComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MainModule,
    HttpClientModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
