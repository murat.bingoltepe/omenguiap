export enum Country
  {
    Turkey = 0,
    Germany = 1,
    Austria = 2
  }

  export namespace Country {

    export function values() {
      return Object.keys(Country).filter(
        (type) => isNaN(<any>type) && type !== 'values'
      );
    }
  }