export enum OrderState
{
  Pending = 0,
  Confirmed = 1,
  Shipped = 2,
  Delivered = 3
}
