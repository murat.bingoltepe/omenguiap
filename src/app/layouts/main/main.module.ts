import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';

import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { AppSettingsComponent } from 'src/app/modules/settings/app-settings/app-settings.component';
import { UserSettingsComponent } from 'src/app/modules/settings/user-settings/user-settings.component';

import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider'
import { MatCardModule } from '@angular/material/card'


import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule } from '@angular/material/table';

@NgModule({
  declarations: [
    MainComponent,
    DashboardComponent,
    AppSettingsComponent,
    UserSettingsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    MatCardModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatTableModule
  ]
})
export class MainModule { }
